/*
 * Your program must print string with the number of years and months and the total number of days between the dates.
 * Dates are provided in dd.mm.yyyy format.
 * You are not allowed to plug in JS libraries such as moment.js or date-fns directly into the code. All code need to be written in this file.
 *
 * Result must be shown as a string in years, months and total days. If years or months are 0, then it should not be displayed in the output.
 *
 * Example:
 * Input: ['01.01.2000', '01.01.2016']
 * Output:
 * '16 years, total 5844 days'
 *
 * Example 2:
 * Input: ['01.11.2015', '01.02.2017']
 *
 * Output:
 * '1 year, 3 months, total 458 days'
 */
const dates = [
  ["01.01.2000", "01.01.2016"],
  ["01.01.2016", "01.08.2016"],
  ["01.11.2015", "01.02.2017"],
  ["17.12.2016", "16.01.2017"],
  ["01.01.2016", "01.01.2016"],
  ["28.02.2015", "13.04.2018"],
  ["28.01.2015", "28.02.2015"],
  ["17.03.2022", "17.03.2023"],
  ["17.02.2024", "17.02.2025"],
];

const dateRegex = /([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{1,4})/;

const parseDate = (s) => {
  const result = dateRegex.exec(s);

  if (result === null) throw new Error("Unparsable date");
  const [_, dayS, monthS, yearS] = result;

  if ([dayS, monthS, yearS].find((x) => x === undefined)) {
    throw new Error("Data element unparsable");
  }

  const [day, month, year] = [dayS, monthS, yearS].map((x) => parseInt(x, 10));

  if ([day, month, year].find((x) => Number.isNaN(x))) {
    throw new Error("Data element unparsable");
  }

  return new Date(`${month}-${day}-${year}`);
};

// Receive string of dates one after each other
function outputDate(dates) {
  const start = parseDate(dates[0]);
  const end = parseDate(dates[1]);

  const timeInMilli = end.getTime() - start.getTime();

  // measure days
  const days = Math.round(timeInMilli / (1000 * 60 * 60 * 24));

  // measure years
  let years = end.getFullYear() - start.getFullYear();
  if (
    start.getMonth() > end.getMonth() ||
    (start.getMonth() === end.getMonth() && start.getDate() > end.getDate())
  ) {
    years -= 1;
  }

  // measure months
  let months = end.getMonth() - start.getMonth();
  if (months < 0) {
    months += 12;
  }
  if (start.getDate() > end.getDate()) months -= 1;

  return (
    (years > 0 ? `${years} year${years > 1 ? "s" : ""}, ` : "") +
    (months > 0 ? `${months} month${months > 1 ? "s" : ""}, ` : "") +
    `total ${days} day${days !== 1 ? "s" : ""}`
  );
}
